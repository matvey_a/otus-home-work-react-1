import { ChangeEvent, Component, ReactNode } from "react";

class LoginProps {    
}

class LoginState {
    username: string = '';
    password: string = '';
}

export class Login extends Component<LoginProps, LoginState>{

    constructor(props: LoginProps) {
        super(props);
        this.state = { username: '', password: '' };
    }    

    sendToServer = () =>
    {                        
        fetch(`api/Account?login=${this.state.username}&password=${this.state.password}`, {
        method: 'POST',
        headers: {
            'Content-Type': 'application/json',
        },         
        })
        .then((response) => response.json())
        .then((data) => {
            console.log('Success:', data);
        })
        .catch((error) => {
            console.error('Error:', error);
        });

        console.log(this.state.username + ' '  + this.state.password );
    }

    updateUnameValue = (evt: ChangeEvent<HTMLInputElement>) => {
        const val = evt.target.value;
        
        this.setState({
            username: val
        });
      }

    updatePasswordValue = (evt: ChangeEvent<HTMLInputElement>) => {
        const val = evt.target.value;
        
        this.setState({
            password: val
        });
      }

    render(): ReactNode {
        return <>
            <div>    
                <div>
                    <label>Username AAA </label>
                    <input type="text" name="uname" onChange={evt => this.updateUnameValue(evt)}  required />        
                </div>
            
                <div >
                    <label>Password </label>
                    <input type="password" name="pass" onChange={evt => this.updatePasswordValue(evt)} required />        
                </div>
                
                <div>
                    <input type="submit" onClick={this.sendToServer} />
                </div>
                
            </div>
        </>           
        ;
    }
}