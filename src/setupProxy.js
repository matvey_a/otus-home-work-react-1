const { createProxyMiddleware } = require('http-proxy-middleware');

module.exports = function(app) {
    app.use(
        '/api',
        createProxyMiddleware({
          target: 'https://mtest1092.azurewebsites.net',
          changeOrigin: true,
        })
      );
};